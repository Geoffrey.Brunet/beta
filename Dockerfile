#
# Build stage
#
FROM maven:3.6.3-openjdk-17-slim AS build
WORKDIR /home/beta
COPY src /home/beta/src
COPY pom.xml /home/beta
RUN mvn -f /home/beta/pom.xml clean package -DskipTests

#
# Package stage
#
FROM openjdk:17.0-slim
COPY --from=build /home/beta/target/beta-0.0.1-SNAPSHOT.jar /usr/local/lib/beta-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/beta-0.0.1-SNAPSHOT.jar"]
